const jwt = require('jsonwebtoken');

const { NotAuthorizedError } = require('../utils/errors');

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  try {
    if (!authorization) {
      throw new NotAuthorizedError('Please, provide "authorization header"');
    }
    const [, token] = authorization.split(' ');
    if (!token) {
      throw new NotAuthorizedError('Please, provide token to request');
    }

    const tokenPayload = await jwt.verify(token, 'secret');
    req.user = {
      userId: tokenPayload._id,
      email: tokenPayload.email,
    };
    next();
  } catch (err) {
    res.status(err.status).json({ message: err.message });
  }
};

module.exports = {
  authMiddleware,
};
