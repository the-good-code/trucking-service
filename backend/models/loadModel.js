const mongoose = require('mongoose');

const logsSchema = new mongoose.Schema({
  message: String,
  time: {
    type: Date,
    default: Date.now(),
  },
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    select: false,
  },
});

const loadShema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery'],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: [logsSchema],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  __v: {
    type: Number,
    select: false,
  },
});

const Load = mongoose.model('Load', loadShema);
const Log = mongoose.model('Log', logsSchema);


module.exports = { Load, Log };
