const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/userModel');

const createProfile = async ({ email, password, role }) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();
};
const login = async ({ email, password }) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('No user found');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid password');
  }

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, 'secret');
  return token;
};

module.exports = {
  createProfile,
  login,
};
