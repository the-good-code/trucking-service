const { Load, Log } = require('../models/loadModel');

const addUserLoad = async (userId, loadPayload) => {
  const load = new Load({ ...loadPayload, created_by: userId });
  console.log(load);
  await load.save();
};

const getloadsByShipper = async (userId, offset, limit, status) => {
  const limitNumb = +limit;
  let validatedLimit;

  if (!limitNumb || Math.round(limitNumb) < 10) {
    validatedLimit = 10;
  } else if (Math.round(limitNumb) > 50) {
    validatedLimit = 50;
  } else {
    validatedLimit = Math.round(limitNumb);
  }

  const validatedOffset = +offset ? Math.round(+offset) : 0;


  const filter = {
    $or: [
      { status: 'POSTED' },
      {
        $and: [
          { assigned_to: req.user.id },
        ],
      },
    ],
  };
  if (status) {
    filter.$or[1].$and.push({ status: status });
  }
  const loads = await Load
      .find(filter)
      .skip(validatedOffset)
      .limit(validatedLimit);

  const count = await Load.count(filter);


  return { validatedOffset, validatedLimit, count, loads };
};
const getloadsByDriver = async (userId, offset, limit, status) => {
  const limitNumb = +limit;
  const offsetNumb = +offset;
  let validatedLimit;
  let validatedOffset;

  if (!limitNumb || Math.round(limitNumb) < 10) {
    validatedLimit = 10;
  } else if (Math.round(limitNumb) > 50) {
    validatedLimit = 50;
  } else {
    validatedLimit = Math.round(limitNumb);
  }

  !offsetNumb ? validatedOffset = 0 : validatedOffset = Math.round(offsetNumb);

  const notes = await Note.find({ assigned_to: userId, status: status })
      .skip(validatedOffset)
      .limit(validatedLimit);

  const count = await Note.count({ userId });


  return { validatedOffset, validatedLimit, count, notes };
};

module.exports = {
  addUserLoad,
  getloadsByShipper,
  getloadsByDriver,
};

