const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/userModel');

const getProfileInfo = async (userId) => {
  const user = await User.findOne({ _id: userId }, '-password');

  return user;
};

const deleteProfile = async (userId) => {
  await User.remove({ _id: userId });
};

const changeProfilePassword = async (username, oldPassword, newPassword) => {
  const user = await User.findOne({ username });

  if (!user) {
    throw new Error('No user found');
  }

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password');
  }

  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, 'secret');
  return token;
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
